Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Math-Derivative
Upstream-Contact: John M. Gamble <jgamble@cpan.org>
Source: https://metacpan.org/release/Math-Derivative

Files: *
Copyright: 1995, John A.R. Williams <J.A.R.Williams@aston.ac.uk>
 2017, John M. Gamble <jgamble@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2005, 2006, Carlo Segre <segre@iit.edu>
 2006, Carlo Segre <segre@debian.org>
 2006, Frank Lichtenheld <djpig@debian.org>
 2007, gregor herrmann <gregoa@debian.org>
 2008, Joachim Breitner <nomeata@debian.org>
 2011, Fabrizio Regalli <fabreg@fabreg.it>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
